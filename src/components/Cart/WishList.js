import React, { useState, useEffect } from "react";
import styles from "./Cart.module.css";

import { connect } from "react-redux";
import WishItem from "./WishItem";

//import CartItem from "./CartItem/CartItem";

const WishList = ({ wishlist }) => {
  

  return (
    <div className={styles.cart}>
      <div className={styles.cart__items}>
        {wishlist.map((item) => (
          <WishItem key = {item.id} item={item}/>
        ))}
      </div>
      
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    wishlist: state.shop.wishlist,
  };
};

export default connect(mapStateToProps)(WishList);
