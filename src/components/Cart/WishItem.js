import React, { useState } from "react";
import styles from "./WishItem.module.css";

import { connect } from "react-redux";
import {
  adjustItemQty,
  removeFromCart,removeFromWishlist,addToCart
} from "../../redux/Shopping/shopping-actions";

const WishItem = ({ item,  removeFromWishlist,adjustQty,addToCart }) => {
    console.log(item,"item");
  //const [input, setInput] = useState(item.qty);
  const [input, setInput] = useState(item.qty);

  const onChangeHandler = (e) => {
    setInput(e.target.value);
    adjustQty(item.id, e.target.value);
  };

 

  return (
    <div style={{marginBottom: "1rem",
        display: "flex",
        boxShadow: "0 2px 8px rgba(0, 0, 0, 0.26)",
        borderRadius: "10px"}}>
      <img
        style={{width: "350px",
            objectFit: "contain",
            borderRadius: "10px"}}
        src={item.image}
        alt={item.title}
      />
      <div style={{padding: "1rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    color: "var(--secondary-color)"}}>
        <p style={{fontSize: "1rem",
    fontWeight: "bold"}}>{item.title}</p>
        <p style={{ fontSize: "0.8rem"}}>{item.description}</p>
        <p style={{fontSize: "1rem",
    fontWeight: "bold"}}>$ {item.price}</p>
      </div>
      <div style={{display: "flex",
  flexDirection: "column",
  justifyContent: "space-around",
  alignItems: "center",
  padding: "0.6rem"}}>
      
        
        <button
          onClick={() => removeFromWishlist(item.id)}
          style={{width: "100px",
          padding: "0.5rem",
          border: "1px solid var(--secondary-color)",
          borderRadius: "10px",
          fontSize: "0.65rem",
          fontWeight: "bold",
          cursor: "pointer"}}
       
        >
            REMOVE
          
          
        </button>
        <button
          onClick={() => addToCart(item.id)}
          style={{width: "100px",
            padding: "0.5rem",
            border: "1px solid var(--secondary-color)",
            borderRadius: "10px",
            fontSize: "0.65rem",
            fontWeight: "bold",
            cursor: "pointer"}}
        >
            Add to Cart
          
        </button>
        

      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    adjustQty: (id, value) => dispatch(adjustItemQty(id, value)),
    addToCart: (id) => dispatch(addToCart(id)),
    
    removeFromWishlist: (id) => dispatch(removeFromWishlist(id)),


  };
};

export default connect(null, mapDispatchToProps)(WishItem);
