import React from "react";
//import ProductArray from "./redux/Shopping/ProductArray";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import "./App.css";

import { connect } from "react-redux";

import Navbar from "./components/Navbar/Navbar";
import Products from "./components/Products/Products";
import Cart from "./components/Cart/Cart";
import SingleItem from "./components/SingleItem/SingleItem";
import WishList from "./components/Cart/WishList";
import {useState} from "react";

function App({ current,cart,wishlist }) {
  const [data,SetData] = useState(false);
 
  return (
    <Router>
      <div className="app">
        <Navbar />
        
        <Switch>
          <Route exact path="/" component={Products} />
          
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/wishlist" component={WishList} />
          <Route exact path="/product/:id" component={SingleItem} />
          
         
         {!current ? (
            <Redirect to="/" />
          ) : (
            <Route exact path="/product/:id" component={SingleItem} />
          )}
          

          


        </Switch>
      </div>
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    current: state.shop.currentItem,
    cart:state.shop.cart,
    WishList:state.shop.wishlist
  };
};

export default connect(mapStateToProps)(App);
